# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

"""Piston/recoater servo abstraction"""

# Built-in
import logging
import threading
import time


class ServoEnableError(Exception):
    """Raised when a servo could not be enabled"""


class Servo:
    """Used to enable/move/disable a piston/recoater servo.

    Keeps state with OPC server for servos controlling pistons and
    recoater.
    """

    def __init__(self, opc, name, zero_position=0):
        self.opc = opc
        self.name = name
        # The `zero_position` is set in gRPC `PistonOffsetControl` call
        self.zero_position = zero_position
        self.enabled = False

        logger_name = f"{name.title()}{self.__class__.__name__}"
        self.log = logging.getLogger(logger_name)
        # Only one thread is allowed to change a servo at a time
        self._lock = threading.RLock()

    def __repr__(self):
        return (
            f"<{self.__class__.__name__}: {self.name}, "
            f"enabled={self.enabled}, zero_pos={self.zero_position}>"
        )

    def disable(self):
        """Convenience method to disable a servo"""

        with self._lock:
            self.log.info("Disabling servo")
            self.opc.set_bool(f"Servo:{self.name}Enable", False)
            self.enabled = False

    def enable(self):
        """Enable a servo

        In order to enable the servo we first must check the status
        flag of the servo. If this is false, we "reset" the servo by
        first setting enabled->false and then back to enabled->true.
        After the toggle we check the status again, and if it still
        false, something is wrong.

        Raises `ServoEnableError` if servo could not be enabled.
        """

        with self._lock:
            # If already enabled - do nothing
            self.log.info("Enabling servo ...")
            servo_status = self.opc.get_bool(f"Servo:{self.name}Status")
            if servo_status:
                self.log.info("Servo already enabled. Done.")
                self.enabled = True
                return

            # Reset servo by creating a rising edge of the
            # corresponding *Enable variable.
            self.log.info("Resetting servo ...")
            self.opc.set_bool(f"Servo:{self.name}Enable", False)
            time.sleep(0.1)  # Wait for command to take hold
            # Now enable the servo
            self.opc.set_bool(f"Servo:{self.name}Enable", True)
            time.sleep(0.1)  # 100 ms too long?
            self.log.info("Done resetting servo.")

            # Check status again
            servo_status = self.opc.get_bool(f"Servo:{self.name}Status")
            if not servo_status:
                raise ServoEnableError(f"Bad status after {self.name} servo reset")

            self.log.info("Servo successfully enabled.")
            self.enabled = True

    def position(self) -> float:
        """Return current position"""
        return self.opc.get_float(f"Servo:{self.name}Position")

    def move(self, position: float, speed: float, acc: float = 0):
        """Move to specified `position` with specified `speed`.

        `acc` currently not implemented.
        """

        if not self.enabled:
            self.log.debug("Move requested but the servo is not enabled.")
            self.enable()

        with self._lock:
            curr_pos = self.position()
            self.log.info("Moving from %.3f to %.3f (absolute)", curr_pos, position)
            self.opc.set_double(f"Servo:RecoaterMoveSpeed", speed)
            self.opc.set_double(f"Servo:{self.name}ABSMovePosition", position)
            self.opc.set_bool(f"Command:{self.name}AbsoluteMove", True)


class ServoWatchdog(threading.Thread):
    """Disable servos when not used for a while"""

    def __init__(self, *servos, timeout=900, **kwargs):
        super().__init__(**kwargs)
        self.servos = servos
        self.timeout = timeout  # Seconds
        self._cond = threading.Condition()
        self.stop_event = threading.Event()
        self.log = logging.getLogger(self.__class__.__name__)

    def stop(self):
        """Stop watchdog thread"""
        self.stop_event.set()
        self.kick()

    def kick(self):
        """Kick the watchdog and make it wait for timeout"""
        with self._cond:
            self._cond.notify()

    def run(self):
        """Wait and disable servos on timeout.

        If kicked, start over and wait for timeout again.
        """
        self.log.info("Servo watchdog thread started.")
        while not self.stop_event.is_set():
            with self._cond:
                if self._cond.wait(self.timeout):
                    continue
                self.log.info("Watchdog timeout! Disabling servos ...")
                for servo in self.servos:
                    try:
                        servo.disable()
                    except Exception:
                        self.log.exception("Failed to disable: %r", servo)
                self.log.info("Done disabling servos.")
        self.log.info("Servo watchdog thread loop completed.")
