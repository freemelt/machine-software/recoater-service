# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

# Built-in
import unittest
import pkgutil
import importlib

# Package
import recoaterservice


class TestImports(unittest.TestCase):
    """Test if all subpackages and modules of recoaterservice can be imported"""

    def test_recoaterservice_imports(self):
        def onerror(m):
            raise ImportError(m)

        for m in pkgutil.walk_packages(
            recoaterservice.__path__, prefix=recoaterservice.__name__ + ".", onerror=onerror
        ):
            print("import", m.name)
            importlib.import_module(m.name)
