# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

# Built-in imports
import enum
import logging
import struct
import time

# PyPI
import grpc
from asyncua.ua.uatypes import ExtensionObject, NodeId, NodeIdType

# Project imports
from .freemeltapi import RecoaterServiceRpc_pb2, RecoaterServiceRpc_pb2_grpc
from .metricspub import MetricsPublisher
from .servo import Servo, ServoWatchdog, ServoEnableError

LOG = logging.getLogger(__name__)

# TODO: What are good defaults? Also, a boolean flag in the proto
#       message will be needed if non-zero default values are desired.
#       See: https://developers.google.com/protocol-buffers/docs/proto3#default
# ServoDefaultAcceleration = 0
# acc = request.acceleration if request.acceleration is not None else ServoDefaultAcceleration


class LayerFeedStatus(enum.Enum):
    COMPLETE = 0
    TRUNCATED = 1
    ILLEGAL = 2
    ERROR = 3
    INIT = 4


class Servicer(RecoaterServiceRpc_pb2_grpc.RecoaterServiceServicer):

    def __init__(self, cfg, opc_client, mqtt_client):
        super().__init__()
        self.cfg = cfg
        self.opc = opc_client
        self.mqtt = mqtt_client

        # We need to share these between the threads. Risk for race
        # condition is small since the threads will update different
        # parameters.
        self.servo_build = Servo(opc_client, 'Build')
        self.servo_powder = Servo(opc_client, 'Powder')
        self.servo_recoater = Servo(opc_client, 'Recoater')

        watchdog_timeout = self.cfg.get_float("Criteria:ServoInactivity_timeout")

        # The watchdog will disable servos when not used for a while
        self.servo_watchdog = ServoWatchdog(
            self.servo_build, self.servo_powder, self.servo_recoater,
            timeout=watchdog_timeout, name='ThreadWatchdog', daemon=True
        )

        # Publishing thread
        self.topic_base = self.cfg['Service:MQTT:TopicBase']

        self.proheat_installed = self.cfg.get("ProHeat:IsInstalled", default=None)
        self.metrics_pub = MetricsPublisher(
            opc_client, mqtt_client, self.topic_base,
            self.servo_build, self.servo_powder,
            proheat_installed=self.proheat_installed,
            interval=1, name='ThreadPub', daemon=True)

    def __enter__(self):
        self.metrics_pub.start()
        self.servo_watchdog.start()
        self.init_proheat()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        LOG.info('Stopping service threads ...')
        self.metrics_pub.stop()
        self.servo_watchdog.stop()
        self.metrics_pub.join()
        self.servo_watchdog.join()
        LOG.info('Stopped service threads.')

    def init_proheat(self):
        """Configure how the PLC should control the proheat plate"""
        if self.proheat_installed is None:
            # ProHeat is absent or the PLC doesn't support it.  Abort.
            return
        if self.proheat_installed:
            b = self.cfg.get_bool("ProHeat:AutomaticLayerFeedControl")
            self.opc.set_bool("ProHeat:AutomaticLayerFeedControl", b)
            b = self.cfg.get_bool("ProHeat:SyncPointControl")
            self.opc.set_bool("ProHeat:SyncPointControl", b)
            self.opc.set_bool("ProHeat:IsInstalled", True)
        else:
            self.opc.set_bool("ProHeat:IsInstalled", False)

    def layerfeed_object(self, body=None):
        """Correct type for a layerfeed object"""

        type_id = NodeId(5002, self.opc.namespace_idx, NodeIdType.FourByte)
        try:
            ext = ExtensionObject(type_id, body)
        except TypeError as error:
            assert "__init__() takes 1 positional argument" in str(error), error
            # Fallback to old behaviour in asyncua 0.6.1 where
            # ExtensionObject doesn't take any arguments:
            ext = ExtensionObject()
            # The namespace idx is assumed to the same for the
            # extension object as it is for the variables
            # 5002 is the extension object type used for LayerFeedParams
            ext.TypeId = type_id
            ext.Body = body
            ext.Encoding = 1
        return ext

    def get_layerfeed_status(self):
        layer_status_struct = self.opc.get_struct("Command:LayerFeedStatus")
        status = struct.unpack("H", layer_status_struct.Body[0:2])[0]
        return LayerFeedStatus(status)

    #
    # RPC Callbacks
    #
    def RecoatCycleControl(self, base_request, context):
        LOG.info('RecoatCycleControl called. Params: %r', base_request)
        # LayerFeedParam_CMD
        request = base_request.basic_recoat_cycle
        LOG.debug(f"RecoatCycleControl: {request}")
        self.servo_watchdog.kick()

        # Wait for a trigger?
        if request.triggered_start:

            trig = self.cfg.get_str("OPC:Command:TriggerSource")
            if trig not in {'BeamPowerLow', 'PatternActive'}:
                err_msg = f"Invalid trigger source: {trig!r}"
                LOG.error(err_msg)
                context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
                context.set_details(err_msg)
                return RecoaterServiceRpc_pb2.CommandStatus()
            trig_timeout = self.cfg.get_float("Criteria:TriggerStart_timeout")
            LOG.info("Trigger requested. Source: %r, timeout used: %r.",
                     trig, trig_timeout)

            # Return RPC call after trig is complete
            deadline = time.monotonic() + trig_timeout

            # The trigger source `BeamPowerLow` is implicit.
            beam_current = self.opc.get_float("Beam:Current")
            current_limit = self.cfg.get_float("Criteria:BeamPowerLow")

            if trig == "PatternActive":
                pattern_active_status = self.opc.get_bool("Command:PatternActiveStatus")
                # Wait until pattern active is LOW/FALSE, pattern is then complete
                while pattern_active_status:
                    time.sleep(0.05)  # Poll time, 20Hz
                    pattern_active_status = self.opc.get_bool("Command:PatternActiveStatus")
                    if time.monotonic() >= deadline:
                        err_msg = 'Trigger source PatternActive timed out'
                        LOG.error(err_msg)
                        context.set_code(grpc.StatusCode.DEADLINE_EXCEEDED)
                        context.set_details(err_msg)
                        return RecoaterServiceRpc_pb2.CommandStatus()
                LOG.info('Trigger condition for PatternActive was successful.')

            # Wait until our condition is met
            while beam_current > current_limit:
                time.sleep(0.05) # Poll time, 20Hz
                beam_current = self.opc.get_float("Beam:Current")
                if time.monotonic() >= deadline:
                    err_msg = "Trigger source BeamPowerLow timed out"
                    LOG.error(err_msg)
                    context.set_code(grpc.StatusCode.DEADLINE_EXCEEDED)
                    context.set_details(err_msg)
                    return RecoaterServiceRpc_pb2.CommandStatus()
            LOG.info('Trigger condition for BeamPowerLow was successful.')

        # Create BYTE arrays with LayerFeed parameters
        body = struct.pack(
            'fffffhhh',
            request.powder_piston_distance,  # mm
            request.build_piston_distance,  # mm
            request.recoater_advance_speed,  # mm/s
            request.recoater_retract_speed,  # mm/s
            request.recoater_dwell_time,  # ms
            request.recoater_full_repeats,
            request.recoater_build_repeats,
            1  # TODO: THIS VALUE (MotionMode) WILL BE REMOVED, Adding 0 for backwards compatibility
        )

        # Convert LayerFeed parameter array to replacement BODY
        LOG.debug('LayerFeed body: %r (hex)', body.hex())
        feed_params = self.layerfeed_object(body)
        self.opc.set_struct(f"Command:LayerFeed", feed_params)

        # LayerFeed Status struct
        # StatusType, first 2 bytes
        #  0 -> Motion complete
        #  1 -> Motion truncated
        #  2 -> Motion illegal
        #  3 -> Motion error
        #  4 -> Motion init
        # BuildTablePosition, double 8 bytes
        # PowderTablePosition, double 8 bytes

        layer_feed_stopped = False
        # Wait until wall shutter is open to make sure the sequence started
        while not self.opc.get_bool("WallShutter:IsOpen") and not layer_feed_stopped:
            time.sleep(0.05)
            if self.get_layerfeed_status() != LayerFeedStatus.INIT:
                # Layer feed stopped before wall shutter closed was detected
                layer_feed_stopped = True

        # Wait until shutter closed
        while self.opc.get_bool("WallShutter:IsOpen") and not layer_feed_stopped:
            time.sleep(0.05)
            if self.get_layerfeed_status() != LayerFeedStatus.INIT:
                # Layer feed stopped before wall shutter closed was detected
                layer_feed_stopped = True

        # Log how the layerfeed sequence finnished
        lf_status = self.get_layerfeed_status()
        if (layer_feed_stopped):
            if lf_status != LayerFeedStatus.COMPLETE:
                err_msg = f'LayerFeed did not complete. MOTION {lf_status.name}.'
                LOG.error(err_msg)
                context.set_code(grpc.StatusCode.INTERNAL)
                context.set_details(err_msg)
            else:
                LOG.info('LayerFeed returned %r', lf_status)
        else:
            LOG.info('Wall shutter successfully closed. LayerFeed complete. %r', lf_status)

        return RecoaterServiceRpc_pb2.CommandStatus()

    def _servo_control_request(self, request):
        """Unpack the `oneof` used in the request

        Return both the parameter and the value"""
        type_ = request.WhichOneof('request')
        pos = getattr(request, type_)
        LOG.debug(f"Position Type: {type_}:{pos}")
        return (type_, pos)

    def _servo_move(self, servo, pos, speed, context):
        self.servo_watchdog.kick()
        try:
            servo.enable()
        except ServoEnableError as error:
            LOG.error(str(error))
            context.set_code(grpc.StatusCode.ABORTED)
            context.set_details(str(error))
        else:
            servo.move(pos, speed, acc=0)
        return RecoaterServiceRpc_pb2.CommandStatus()

    def _servo_homing_request(self, request):
        """Unpack the `oneof` used in the request

        Return both the parameter and the value"""
        type_ = request.WhichOneof('request')
        value = getattr(request, type_)
        LOG.debug(f"Homing servo: {type_}:{value}")
        return (type_, value)

    def BuildPistonPositionControl(self, request, context):
        LOG.info('BuildPistonPositionControl called. Params:\n%r', request)
        position_type, pos = self._servo_control_request(request)

        if position_type == "target_process_position":
            # Process position is relative the zero_position
            pos = self.servo_build.zero_position - pos
        elif position_type == "relative_distance":
            pos += self.servo_build.position()
        return self._servo_move(self.servo_build, pos, request.speed, context)

    def PowderPistonPositionControl(self, request, context):
        LOG.info('PowderPistonPositionControl called. Params:\n%r', request)
        position_type, pos = self._servo_control_request(request)

        if position_type == "target_process_position":
            # Process position is relative the zero_position
            pos = pos + self.servo_powder.zero_position
        elif position_type == "relative_distance":
            # Read the current servo position
            pos += self.servo_powder.position()
        return self._servo_move(self.servo_powder, pos, request.speed, context)

    def RecoaterPositionControl(self, request, context):
        LOG.info('RecoaterPositionControl called. Params:\n%r', request)
        position_type, pos = self._servo_control_request(request)

        if position_type == "relative_distance":
            pos += self.servo_recoater.position()
        return self._servo_move(self.servo_recoater, pos, request.speed, context)

    def PistonOffsetControl(self, request, context):
        LOG.info('PistonOffsetControl called. Params:\n%r', request)
        build_offset = request.build_offset
        powder_offset = request.powder_offset

        if request.relative:
            # Calculate the servo positions from the top of the
            # cylinder. Add the offset value in the request to the
            # servo position.
            build_offset += self.opc.get_float("Servo:BuildPosition")
            powder_offset += self.opc.get_float("Servo:PowderPosition")
        else:
            # Take the offset value and use as servo offset, regardless
            # of the current servo position.
            pass

        self.servo_build.zero_position = build_offset
        self.servo_powder.zero_position = powder_offset
        LOG.info('Build servo zerio position set to %f', build_offset)
        LOG.info('Powder servo zerio position set to %f', powder_offset)
        return RecoaterServiceRpc_pb2.CommandStatus()

    def HaltControl(self, request, context):
        # TODO: Missing OPC variables
        raise NotImplementedError()
        return RecoaterServiceRpc_pb2.CommandStatus()

    def WallShutter(self, request, context):
        LOG.info('WallShutter called. Params:\n%r', request)
        self.opc.set_bool('WallShutter:Open', request.open)
        return RecoaterServiceRpc_pb2.CommandStatus()

    def AllServoReset(self, request, context):
        LOG.info('AllServoReset called. Params:\n%r', request)
        self.opc.set_bool('Servo:AllReset', True)
        return RecoaterServiceRpc_pb2.CommandStatus()

    def ServoZeroPosition(self, request, context):
        LOG.info('ServoZeroPosition called. Params:\n%r', request)
        self.opc.set_bool('Servo:BuildZeroPos', request.build)
        self.opc.set_bool('Servo:PowderZeroPos', request.powder)
        self.opc.set_bool('Servo:RakeZeroPos', request.recoater)
        return RecoaterServiceRpc_pb2.CommandStatus()

    def ServoHoming(self, request, context):
        LOG.info('ServoHoming called. Params:\n%r', request)
        servo, value = self._servo_homing_request(request)

        # Which servo should we start the homing process for?
        if servo == "build":
            self.opc.set_bool('Command:BuildHoming', value)
        elif servo == "powder":
            self.opc.set_bool('Command:PowderHoming', value)
        return RecoaterServiceRpc_pb2.CommandStatus()

    def ServoHomingTorque(self, request, context):
        LOG.info('ServoHomingTorque called. Params:\n%r', request)
        self.opc.set_float('Servo:BuildHomingTorque', request.build_homing_torque)
        self.opc.set_float('Servo:PowderHomingTorque', request.powder_homing_torque)
        self.opc.set_float('Servo:BuildHomingOffset', request.build_homing_offset)
        self.opc.set_float('Servo:PowderHomingOffset', request.powder_homing_offset)
        return RecoaterServiceRpc_pb2.CommandStatus()

    def ProHeatControl(self, request, context):
        """Move ProHeat plate to heating/home-position"""
        LOG.info('ProHeatControl called. Params:\n%r', request)
        if not self.proheat_installed:
            error = "ProHeat is not installed"
            LOG.error(str(error))
            context.set_code(grpc.StatusCode.ABORTED)
            context.set_details(str(error))
        elif request.heat:
            self.opc.set_bool('ProHeat:ManualControlHome', False)
            # Triggers on rising edge
            self.opc.set_bool('ProHeat:ManualControlHeating', False)
            self.opc.set_bool('ProHeat:ManualControlHeating', True)
        else:
            self.opc.set_bool('ProHeat:ManualControlHeating', False)
            # Triggers on rising edge
            self.opc.set_bool('ProHeat:ManualControlHome', False)
            self.opc.set_bool('ProHeat:ManualControlHome', True)
        return RecoaterServiceRpc_pb2.CommandStatus()
