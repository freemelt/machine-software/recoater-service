# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

"""Continuously read sensor data and publish to mqtt broker"""

# Project
from servicelib.mqtt import MQTTMessage, BaseMetricsPublisherThread


class MetricsPublisher(BaseMetricsPublisherThread):
    """Continuously publish metrics to mqtt broker."""

    def __init__(
            self, opc_client, mqtt_client, topic_base, build_state, powder_state,
            proheat_installed=False, **kwargs
    ):
        super().__init__(mqtt_client, **kwargs)
        self.opc = opc_client
        self.topic_base = topic_base

        self.build_state = build_state
        self.powder_state = powder_state
        self.proheat_installed = proheat_installed

    def get_messages(self):
        messages = list()
        messages += self.read_servos()
        messages += self.read_proheat()
        return messages

    def read_servos(self):
        self.log.debug("Reading OPC variables of Piston/Recoater location/speed")
        build_position = self.opc.get_float("Servo:BuildPosition")
        powder_position = self.opc.get_float("Servo:PowderPosition")

        # Calculate the process positions
        # Build process increases downwards
        # Powder process increases upwards
        build_process_position = self.build_state.zero_position - build_position
        powder_process_position = powder_position - self.powder_state.zero_position

        topic = f"{self.topic_base}/Recoater/Position"
        m1 = MQTTMessage(
            f"{topic}/BuildServo",
            {
                "Status": self.opc.get_bool("Servo:BuildStatus"),
                "Location": build_position,
                "ProcessLocation": build_process_position,
                "HomingStatus": self.opc.get_bool("Servo:BuildHomingStatus"),
            },
        )
        m2 = MQTTMessage(
            f"{topic}/PowderServo",
            {
                "Status": self.opc.get_bool("Servo:PowderStatus"),
                "Location": powder_position,
                "ProcessLocation": powder_process_position,
                "HomingStatus": self.opc.get_bool("Servo:PowderHomingStatus"),
            },
        )
        m3 = MQTTMessage(
            f"{topic}/RecoaterServo",
            {
                "Status": self.opc.get_bool("Servo:RecoaterStatus"),
                "Location": self.opc.get_float("Servo:RecoaterPosition"),
                "Speed": self.opc.get_float("Servo:RecoaterMoveSpeed"),
            },
        )
        m4 = MQTTMessage(
            f"{self.topic_base}/Recoater/WallShutter/0",
            {"Open": self.opc.get_bool("WallShutter:IsOpen")},
        )

        # racc_path = self.cfg.get_str("Config:OPC:Servo:RecoaterMoveAcceleration")
        # m7 = point(timestamp, "Recoater", {"Acceleration": self.opc.get_float(racc_path)}, {"Position": "RecoaterServo"})
        return [m1, m2, m3, m4]

    def read_proheat(self):
        self.log.debug("Reading OPC variables of ProHeat")
        topic = f"{self.topic_base}/ProHeat/0"
        if self.proheat_installed:
            m1 = MQTTMessage(
                topic,
                {
                    "AutomaticLayerFeedControl": self.opc.get_bool("ProHeat:AutomaticLayerFeedControl"),
                    "SyncPointControl": self.opc.get_bool("ProHeat:SyncPointControl"),
                    "IsInstalled": self.opc.get_bool("ProHeat:IsInstalled"),
                    "ManualControlHeating": self.opc.get_bool("ProHeat:ManualControlHeating"),
                    "ManualControlHome": self.opc.get_bool("ProHeat:ManualControlHome"),
                    "AtHomePosition": self.opc.get_bool("ProHeat:AtHomePosition"),
                    "AtHeatingPosition": self.opc.get_bool("ProHeat:AtHeatingPosition"),
                    "ActivationError": self.opc.get_bool("ProHeat:ActivationError"),
                    "SyncPoint": self.opc.get_bool("ProHeat:SyncPoint"),
                }
            )
        else:
            m1 = MQTTMessage(
                topic,
                {
                    "IsInstalled": self.proheat_installed,
                }
            )
        return [m1]


    # def readTemperatures(self, timestamp):
    #     # Encoder Temperature
    #     pe_temp_path = self.cfg.get_str("Config:OPC:Metrics:PowderServoEncoderTemperature")
    #     be_temp_path = self.cfg.get_str("Config:OPC:Metrics:BuildServoEncoderTemperature")
    #     re_temp_path = self.cfg.get_str("Config:OPC:Metrics:RecoaterServoEncoderTemperature")

    #     # Driver Temperature
    #     pd_temp_path = self.cfg.get_str("Config:OPC:Metrics:PowderServoDriverTemperature")
    #     bd_temp_path = self.cfg.get_str("Config:OPC:Metrics:BuildServoDriverTemperature")
    #     rd_temp_path = self.cfg.get_str("Config:OPC:Metrics:RecoaterServoDriverTemperature")

    #     # TODO: These do not yet exist in the PLC
    #     p1 = point(timestamp, "Recoater", {"Temperature": self.opc.get_float(pe_temp_path)}, {"Position": "PowderServo", "Unit":"Encoder"})
    #     p2 = point(timestamp, "Recoater", {"Temperature": self.opc.get_float(be_temp_path)}, {"Position": "BuildServo", "Unit": "Encoder"})
    #     p3 = point(timestamp, "Recoater", {"Temperature": self.opc.get_float(re_temp_path)}, {"Position": "RecoaterServo", "Unit":"Encoder"})
    #     p4 = point(timestamp, "Recoater", {"Temperature": self.opc.get_float(pd_temp_path)}, {"Position": "PowderServo", "Unit": "Driver"})
    #     p5 = point(timestamp, "Recoater", {"Temperature": self.opc.get_float(bd_temp_path)}, {"Position": "BuildServo", "Unit": "Driver"})
    #     p5 = point(timestamp, "Recoater", {"Temperature": self.opc.get_float(rd_temp_path)}, {"Position": "RecoaterServo", "Unit": "Driver"})

    #     return [p1, p2, p3, p4, p5, p6]

    # def readOverload(self, timestamp):
    #     # Overload percentage
    #     poverload_path = self.cfg.get_str("Config:OPC:Metrics:PowderServoOverload")
    #     boverload_path = self.cfg.get_str("Config:OPC:Metrics:BuildServoOverload")
    #     roverload_path = self.cfg.get_str("Config:OPC:Metrics:RecoaterServoOverload")

    #     p1 = point(timestamp, "Recoater", {"Overload": self.opc.get_float(poverload_path)}, {"Position": "BuildServo"})
    #     p2 = point(timestamp, "Recoater", {"Overload": self.opc.get_float(boverload_path)}, {"Position": "PowderServo"})
    #     p3 = point(timestamp, "Recoater", {"Overload": self.opc.get_float(roverload_path)}, {"Position": "RecoaterServo"})
    #     return [p1, p2, p3]
