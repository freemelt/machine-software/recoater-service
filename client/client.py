# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only


# Test client to create different RPC commands and send to the service, used for development.

# if __name__ == '__main__':

# with grpc.insecure_channel('vtservice:31304') as channel:
import grpc
import recoaterservice.freemeltapi.RecoaterServiceRpc_pb2 as msg
import recoaterservice.freemeltapi.RecoaterServiceRpc_pb2_grpc as rpc

channel = grpc.insecure_channel("0.0.0.0:31302")
stub = rpc.RecoaterServiceStub(channel)

# stub.PistonOffsetControl(RecoaterServiceRpc_pb2.PistonOffsetCommand(relative=True, buildOffset=-10.0, powderOffset=-10.0))
# stub.PistonOffsetControl(RecoaterServiceRpc_pb2.PistonOffsetCommand(relative=False, buildOffset=90.0, powderOffset=20.0))
# stub.RecoaterPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(relativeDistance=True, targetPhysicalPosition=123.0, speed=4.0))
# stub.RecoaterPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(relativeDistance=False, targetPhysicalPosition=123.0, speed=4.0))
# # Relative movement - Physical
# stub.PowderPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(relativeDistance=True, targetPhysicalPosition=10.0, speed=4.0))
# stub.BuildPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(relativeDistance=True, targetPhysicalPosition=123.0, speed=4.0))
# # - Process
# stub.PowderPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(relativeDistance=True, targetProcessPosition=10.0, speed=4.0))
# stub.BuildPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(relativeDistance=True, targetProcessPosition=10.0, speed=4.0))
# # Absolute movement - Physical
# stub.PowderPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(relativeDistance=False, targetPhysicalPosition=100.0, speed=4.0))
stub.BuildPistonPositionControl(
    msg.PositionControlCommand(
        relativeDistance=False, targetPhysicalPosition=100.0, speed=4.0
    )
)
# # - Process
# stub.PowderPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(relativeDistance=False, targetProcessPosition=10.0, speed=4.0))
# stub.BuildPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(relativeDistance=False, targetProcessPosition=10.0, speed=4.0))
#
# def layerfeed(trig=False):
#     return stub.RecoatCycleControl(RecoaterServiceRpc_pb2.RecoatCycleCommand(
#         basicRecoatCycle=RecoaterServiceRpc_pb2.BasicRecoatCycle(
#             buildPistonDistance=-1.0,
#             powderPistonDistance=1.0,
#             recoaterAdvanceSpeed=200.0,
#             recoaterRetractSpeed=200.0,
#             recoaterDwellTime=1.0,
#             recoaterFullRepeats=0,
#             recoaterBuildRepeats=0,
#             triggeredStart=trig
#         )
#     ))
