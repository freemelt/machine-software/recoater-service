<!--
SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>

SPDX-License-Identifier: GPL-3.0-only
-->

# Recoater Service

Description: This service is responsible for the moving parts inside the chamber, the recoater and the pistons.

Dependencies:
- python3
- opcualib
- grpcio-tools [pip3]

Generating protobuf messages:  
```
python -m grpc_tools.protoc -I=./protos/ --python_out=./src/ --grpc_python_out=./src ./protos/RecoaterServiceRpc.proto
```

Some code to manually send RPC messages to the RecoaterService
```python
import grpc
import RecoaterServiceRpc_pb2
import RecoaterServiceRpc_pb2_grpc
channel = grpc.insecure_channel('0.0.0.0:31302')
stub = RecoaterServiceRpc_pb2_grpc.RecoaterServiceStub(channel)

# Control Recoat Cycle
stub.RecoatCycleControl(RecoaterServiceRpc_pb2.RecoatCycleCommand(
    basicRecoatCycle=RecoaterServiceRpc_pb2.BasicRecoatCycle(
        buildPistonDistance=1.0,
        powderPistonDistance=-1.0,
        recoaterAdvanceSpeed=3.0,
        recoaterRetractSpeed=1.0,
        recoaterDwellTime=1.0,
        recoaterFullRepeats=0, # +1
        recoaterBuildRepeats=1, # 0 = None
        triggeredStart=False
    )
))

# Control Powder Piston Position
## Relative
stub.PowderPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(
    relativeDistance=10.0,speed = 1.0
))

## Physical
stub.PowderPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(
    targetPhysicalPosition=10.0,speed = 1.0
))

## Process
stub.PowderPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(
    targetProcessPosition=10.0,speed = 1.0
))

# Control Build Piston Position
## Relative
stub.BuildPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(
    relativeDistance=10.0,speed = 1.0
))

## Physical
stub.BuildPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(
    targetPhysicalPosition=10.0,speed = 1.0
))

## Process
stub.BuildPistonPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(
    targetProcessPosition=10.0,speed = 1.0
))

# Control Recoater Position
## Relative
stub.RecoaterPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(
    relativeDistance=10.0,speed = 1.0
))

## Physical
stub.RecoaterPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(
    targetPhysicalPosition=10.0,speed = 1.0
))

## Process
stub.RecoaterPositionControl(RecoaterServiceRpc_pb2.PositionControlCommand(
    targetProcessPosition=10.0,speed = 1.0
))

# Set Piston Offset
## Relative
stub.PistonOffsetControl(RecoaterServiceRpc_pb2.PistonOffsetCommand(
    relative=True,buildOffset=10.0,powderOffset=20.0
))

## Absolute
stub.PistonOffsetControl(RecoaterServiceRpc_pb2.PistonOffsetCommand(
    relative=False,buildOffset=30.0,powderOffset=40.0
))

# Halt system
stub.HaltControl(RecoaterServiceRpc_pb2.HaltControlCommand())


```


# License
Copyright © 2019 - 2021 Freemelt AB <opensource@freemelt.com>

Recoater service is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
