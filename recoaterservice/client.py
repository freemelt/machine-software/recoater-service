# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

# Built-in
from tkinter import *
from tkinter import ttk
import json

# PyPI
import grpc
import paho.mqtt.client as mqtt

# Freemelt
from servicelib import Configurator

# Project
from .freemeltapi import (
    RecoaterServiceRpc_pb2 as msg,
    RecoaterServiceRpc_pb2_grpc as rpc,
)


class MqttFrame(ttk.LabelFrame):
    def __init__(self, master, mqtt_client, topic_base, *args, **kwargs):
        title = self.__class__.__name__.replace("Frame", "")
        super().__init__(master, *args, text=title, **kwargs)
        self.client = mqtt_client
        self.topic_base = topic_base
        self.client.subscribe(f"{topic_base}/#")
        self.client.on_message = self.on_message

        self.service_alive = StringVar(value="Service alive: N/A")

        self.recoater_status = StringVar(value="RecoaterStatus: N/A")
        self.build_status = StringVar(value="BuildStatus: N/A")
        self.powder_status = StringVar(value="PowderStatus: N/A")

        self.recoater_location = StringVar(value="RecoaterLocation: N/A")
        self.build_location = StringVar(value="BuildLocation: N/A")
        self.powder_location = StringVar(value="PowderLocation: N/A")

        self.build_proc = StringVar(value="BuildProcessLocation: N/A")
        self.powder_proc = StringVar(value="PowderProcessLocation: N/A")

        self.create_widgets()
        self.create_grid()

    def create_widgets(self):
        self.lb_service_alive = ttk.Label(self, textvariable=self.service_alive)

        self.lb_build_status = Label(self, textvariable=self.build_status)
        self.lb_powder_status = Label(self, textvariable=self.powder_status)
        self.lb_recoater_status = Label(self, textvariable=self.recoater_status)

        self.lb_build_location = Label(self, textvariable=self.build_location)
        self.lb_powder_location = Label(self, textvariable=self.powder_location)
        self.lb_recoater_location = Label(self, textvariable=self.recoater_location)

        self.lb_build_proc = Label(self, textvariable=self.build_proc)
        self.lb_powder_proc = Label(self, textvariable=self.powder_proc)

    def create_grid(self):
        self.lb_service_alive.grid(column=0, row=0, sticky="W")

        self.lb_build_status.grid(column=0, row=1, sticky="W", pady=(10, 0))
        self.lb_powder_status.grid(column=0, row=2, sticky="W")
        self.lb_recoater_status.grid(column=0, row=3, sticky="W")

        self.lb_build_location.grid(column=0, row=4, sticky="W", pady=(10, 0))
        self.lb_powder_location.grid(column=0, row=5, sticky="W")
        self.lb_recoater_location.grid(column=0, row=6, sticky="W")

        self.lb_build_proc.grid(column=0, row=7, sticky="W", pady=(10, 0))
        self.lb_powder_proc.grid(column=0, row=8, sticky="W")

    def on_message(self, client, userdata, message):
        data = json.loads(message.payload.decode("utf-8"))
        if message.topic == f"{self.topic_base}/ComponentStatus/Status/Current":
            self.service_alive.set(f'Service alive: {data["alive"]}')

        elif message.topic == f"{self.topic_base}/Recoater/Position/RecoaterServo":
            self.recoater_status.set(f'RecoaterStatus: {data["Status"]}')
            self.recoater_location.set(f'RecoaterLocation: {data["Location"]:.3f}')
        elif message.topic == f"{self.topic_base}/Recoater/Position/BuildServo":
            self.build_status.set(f'BuildStatus: {data["Status"]}')
            self.build_location.set(f'BuildLocation: {data["Location"]:.3f}')
            self.build_proc.set(
                f'RecoaterProcessLocation: {data["ProcessLocation"]:.3f}'
            )
        elif message.topic == f"{self.topic_base}/Recoater/Position/PowderServo":
            self.powder_status.set(f'PowderStatus: {data["Status"]}')
            self.powder_location.set(f'PowderLocation: {data["Location"]:.3f}')
            self.powder_proc.set(
                f'RecoaterProcessLocation: {data["ProcessLocation"]:.3f}'
            )


class RecoatCycleFrame(ttk.LabelFrame):
    def __init__(self, master, grpc_client, *args, **kwargs):
        title = self.__class__.__name__.replace("Frame", "")
        super().__init__(master, *args, text=title, **kwargs)
        self.client = grpc_client

        self.buildPistonDistance = DoubleVar(value=0)
        self.powderPistonDistance = DoubleVar(value=0)
        self.recoaterAdvanceSpeed = DoubleVar(value=200)
        self.recoaterRetractSpeed = DoubleVar(value=200)
        self.recoaterDwellTime = DoubleVar(value=500)
        self.recoaterFullRepeats = IntVar(value=0)
        self.recoaterBuildRepeats = IntVar(value=0)
        self.triggeredStart = BooleanVar(value=False)

        self.create_widgets()
        self.create_grid()

    def create_widgets(self):
        # self.sb_voltage = Spinbox(self, width=5, from_=0, to=60, increment=5, textvariable=self.target_voltage)
        # self.lb_voltage = ttk.Label(self, text='Target Voltage (kV)')

        self.sb_buildPistonDistance = Spinbox(
            self, width=12, from_=-1000, to=1000, textvariable=self.buildPistonDistance
        )
        self.sb_powderPistonDistance = Spinbox(
            self, width=12, from_=-1000, to=1000, textvariable=self.powderPistonDistance
        )
        self.sb_recoaterAdvanceSpeed = Spinbox(
            self, width=12, from_=-1000, to=1000, textvariable=self.recoaterAdvanceSpeed
        )
        self.sb_recoaterRetractSpeed = Spinbox(
            self, width=12, from_=-1000, to=1000, textvariable=self.recoaterRetractSpeed
        )
        self.sb_recoaterDwellTime = Spinbox(
            self, width=12, from_=0, to=1000, textvariable=self.recoaterDwellTime
        )
        self.sb_recoaterFullRepeats = Spinbox(
            self, width=12, from_=0, to=1000, textvariable=self.recoaterFullRepeats
        )
        self.sb_recoaterBuildRepeats = Spinbox(
            self, width=12, from_=0, to=1000, textvariable=self.recoaterBuildRepeats
        )
        self.cb_triggeredStart = Checkbutton(
            self, text="triggeredStart", variable=self.triggeredStart
        )
        self.bt_submit = ttk.Button(self, text="Submit", command=self.command)

        self.lb_buildPistonDistance = ttk.Label(self, text="buildPistonDistance (mm)")
        self.lb_powderPistonDistance = ttk.Label(self, text="powderPistonDistance (mm)")
        self.lb_recoaterAdvanceSpeed = ttk.Label(
            self, text="recoaterAdvanceSpeed (mm/s)"
        )
        self.lb_recoaterRetractSpeed = ttk.Label(
            self, text="recoaterRetractSpeed (mm/s)"
        )
        self.lb_recoaterDwellTime = ttk.Label(self, text="recoaterDwellTime (ms)")
        self.lb_recoaterFullRepeats = ttk.Label(self, text="recoaterFullRepeats")
        self.lb_recoaterBuildRepeats = ttk.Label(self, text="recoaterBuildRepeats")

    def command(self):
        cmd = msg.RecoatCycleCommand(
            cycleDefinition=msg.BasicRecoatCycle(
                build_piston_distance=self.buildPistonDistance.get(),
                powder_piston_distance=self.powderPistonDistance.get(),
                recoater_advance_speed=self.recoaterAdvanceSpeed.get(),
                recoater_retract_speed=self.recoaterRetractSpeed.get(),
                recoater_dwell_time=self.recoaterDwellTime.get(),
                recoater_full_repeats=self.recoaterFullRepeats.get(),
                recoater_build_repeats=self.recoaterBuildRepeats.get(),
                triggered_start=self.triggeredStart.get(),
            )
        )
        resp = self.client.RecoatCycleControl(cmd)
        print(resp)

    def create_grid(self):
        self.sb_buildPistonDistance.grid(column=0, row=0, sticky="W")
        self.sb_powderPistonDistance.grid(column=0, row=1, sticky="W")
        self.sb_recoaterAdvanceSpeed.grid(column=0, row=2, sticky="W")
        self.sb_recoaterRetractSpeed.grid(column=0, row=3, sticky="W")
        self.sb_recoaterDwellTime.grid(column=0, row=4, sticky="W")
        self.sb_recoaterFullRepeats.grid(column=0, row=5, sticky="W")
        self.sb_recoaterBuildRepeats.grid(column=0, row=6, sticky="W")
        self.cb_triggeredStart.grid(column=0, row=7, sticky="W")
        self.bt_submit.grid(column=0, row=8, sticky="W")

        self.lb_buildPistonDistance.grid(column=1, row=0, sticky="W")
        self.lb_powderPistonDistance.grid(column=1, row=1, sticky="W")
        self.lb_recoaterAdvanceSpeed.grid(column=1, row=2, sticky="W")
        self.lb_recoaterRetractSpeed.grid(column=1, row=3, sticky="W")
        self.lb_recoaterDwellTime.grid(column=1, row=4, sticky="W")
        self.lb_recoaterFullRepeats.grid(column=1, row=5, sticky="W")
        self.lb_recoaterBuildRepeats.grid(column=1, row=6, sticky="W")


class PositionControlFrame(ttk.LabelFrame):
    def __init__(self, master, grpc_client, *args, **kwargs):
        title = self.__class__.__name__.replace("Frame", "")
        super().__init__(master, *args, text=title, **kwargs)
        self.client = grpc_client

        self.target = StringVar(value="Recoater")
        self.request = StringVar(value="relative_distance")
        self.speed = DoubleVar(value=1)
        self.pos = DoubleVar(value=1)

        self.create_widgets()
        self.create_grid()

    def create_widgets(self):
        self.cb_target = ttk.Combobox(self, textvariable=self.target, state="readonly")
        self.cb_target["values"] = ("Recoater", "BuildPiston", "PowderPiston")
        self.cb_request = ttk.Combobox(
            self, textvariable=self.request, state="readonly"
        )
        self.cb_request["values"] = (
            "relative_distance",
            "target_physical_position",
            "target_process_position",
        )
        self.sb_pos = Spinbox(
            self, width=7, from_=-1000, to=1000, increment=1, textvariable=self.pos
        )
        self.lb_pos = ttk.Label(self, text="Position (mm)")
        self.sb_speed = Spinbox(
            self, width=7, from_=-1000, to=1000, increment=1, textvariable=self.speed
        )
        self.lb_speed = ttk.Label(self, text="Speed (mm/s)")
        self.bt_submit = ttk.Button(self, text="Submit", command=self.command)

    def command(self):
        cmd = msg.PositionControlCommand()
        setattr(cmd, self.request.get(), self.pos.get())
        cmd.speed = self.speed.get()
        method = getattr(self.client, f"{self.target.get()}PositionControl")
        resp = method(cmd)
        print(resp)

    def create_grid(self):
        self.cb_target.grid(column=0, row=0, sticky="W")
        self.cb_request.grid(column=0, row=1, sticky="W")
        self.sb_pos.grid(column=0, row=2, sticky="WE")
        self.lb_pos.grid(column=1, row=2, sticky="W")
        self.sb_speed.grid(column=0, row=3, sticky="WE")
        self.lb_speed.grid(column=1, row=3, sticky="W")
        self.bt_submit.grid(column=0, row=4, columnspan=2, sticky="WE")


class PistonOffsetFrame(ttk.LabelFrame):
    def __init__(self, master, grpc_client, *args, **kwargs):
        title = self.__class__.__name__.replace("Frame", "")
        super().__init__(master, *args, text=title, **kwargs)
        self.client = grpc_client

        self.relative = BooleanVar(value=True)
        self.build_offset = DoubleVar(value=0)
        self.powder_offset = DoubleVar(value=0)

        self.create_widgets()
        self.create_grid()

    def create_widgets(self):
        self.cb_relative = Checkbutton(self, text="Relative", variable=self.relative)
        self.sb_build_offset = Spinbox(
            self, width=7, from_=-1000, to=1000, textvariable=self.build_offset
        )
        self.lb_build_offset = ttk.Label(self, text="Build offset (mm)")
        self.sb_powder_offset = Spinbox(
            self, width=7, from_=-1000, to=1000, textvariable=self.powder_offset
        )
        self.lb_powder_offset = ttk.Label(self, text="Powder offset (mm)")
        self.bt_submit = ttk.Button(self, text="Submit", command=self.command)

    def command(self):
        cmd = msg.PistonOffsetCommand(
            relative=self.relative.get(),
            buildOffset=self.build_offset.get(),
            powderOffset=self.powder_offset.get(),
        )
        resp = self.client.PistonOffsetControl(cmd)
        print(resp)

    def create_grid(self):
        self.cb_relative.grid(column=0, row=0, sticky="W")
        self.sb_build_offset.grid(column=0, row=1, sticky="W")
        self.lb_build_offset.grid(column=1, row=1, sticky="WE")
        self.sb_powder_offset.grid(column=0, row=2, sticky="W")
        self.lb_powder_offset.grid(column=1, row=2, sticky="WE")
        self.bt_submit.grid(column=0, row=3, columnspan=2, sticky="WE")


def main(args):
    # Load service configuration
    cfg = Configurator.load_config("recoaterservice.yaml")

    rpc_host = cfg.get_str("Service:GRPC:IP")
    rpc_port = cfg.get_int("Service:GRPC:Port")
    channel = grpc.insecure_channel(f"{rpc_host}:{rpc_port}")
    grpc_client = rpc.RecoaterServiceStub(channel)

    mqtt_client = mqtt.Client(
        client_id=f"{__package__}_gui", clean_session=True, userdata=(cfg,)
    )
    mqtt_client.connect(host=cfg["Service:MQTT:IP"], port=cfg["Service:MQTT:Port"])
    mqtt_client.loop_start()

    root = Tk()
    root.option_add("*tearOff", FALSE)
    root.title(f"{__package__} client")
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)

    fr_content = ttk.Frame(root, padding=10)
    fr_mqtt = MqttFrame(
        fr_content, mqtt_client, topic_base=cfg["Service:MQTT:TopicBase"]
    )
    fr_rec = RecoatCycleFrame(fr_content, grpc_client)
    fr_pos = PositionControlFrame(fr_content, grpc_client)
    fr_off = PistonOffsetFrame(fr_content, grpc_client)

    _common = dict(sticky="NEWS", padx=5, pady=5)
    fr_content.grid(column=0, row=0, sticky="NSEW", padx=10, pady=10)
    fr_rec.grid(column=0, row=0, **_common)
    fr_pos.grid(column=0, row=1, **_common)
    fr_off.grid(column=0, row=2, **_common)
    fr_mqtt.grid(column=1, row=0, rowspan=3, **_common)

    try:
        root.mainloop()
    finally:
        mqtt_client.disconnect()
        mqtt_client.loop_stop()
        print("bye")
