# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

from setuptools import setup, find_packages
from recoaterservice import __version__

with open("README.md") as readme_file:
    long_description = readme_file.read()

setup(
    name="recoaterservice",
    version=__version__,
    maintainer="Freemelt AB",
    maintainer_email="opensource@freemelt.com",
    description="Control software for metal 3D printers from Freemelt AB",
    long_description=long_description,
    url="https://gitlab.freemelt.com/machine-software/RecoaterServiceOPC",
    packages=find_packages(exclude=["tests"]),
    install_requires=[
        # sudo apt install freemelt-configurator
        # sudo apt install freemelt-opcualib
        "grpcio>=1.25.0",
        "grpcio-tools>=1.20.1",
        "protobuf>=3.9.1",
        "paho-mqtt>=1.4.0",
    ],
    python_requires=">=3.7",
    classifiers=[
        "Environment :: Console",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: CPython",
    ],
)
